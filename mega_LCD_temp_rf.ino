/*
  LiquidCrystal Library - Hello World

 Demonstrates the use a 16x2 LCD display.  The LiquidCrystal
 library works with all LCD displays that are compatible with the
 Hitachi HD44780 driver. There are many of them out there, and you
 can usually tell them by the 16-pin interface.

 This sketch prints "Hello World!" to the LCD
 and shows the time.

  The circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)

 Library originally added 18 Apr 2008
 by David A. Mellis
 library modified 5 Jul 2009
 by Limor Fried (http://www.ladyada.net)
 example added 9 Jul 2009
 by Tom Igoe
 modified 22 Nov 2010
 by Tom Igoe

 This example code is in the public domain.

 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 */

// include the library code:
#include <OneWire.h>
#include <LiquidCrystal.h>
#include <DallasTemperature.h>

#define		LED		13
#define		_enable 43
#define		_rs		47

// Data wire is plugged into pin 2 on the Arduino
#define 	ONE_WIRE_BUS 	16

// RF module
#define		RF_VCC			21
#define		RF_DATA			20
#define		RF_GND			19

#define		LED2			3
#define		LED3			2

static const char _bits[8] = {41, 39, 37, 35, 33, 31, 29, 27};
static byte solidblk[8] = {
  B00000,
  B11111,
  B11111,
  B11111,
  B11111,
  B11111,
  B00000,
};
static byte emptyblk[8] = {
  B00000,
  B11111,
  B10001,
  B10001,
  B10001,
  B11111,
  B00000,
};

// initialize the library with the numbers of the interface pins
//LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
//LiquidCrystal lcd(rs, rw, enable, d0, d1, d2, d3, d4, d5, d6, d7);
//LiquidCrystal   lcd(47, 45,     43, 27, 29, 31, 33);
LiquidCrystal   lcd(47, 45,     43, 41, 39, 37, 35, 33, 31, 29, 27);
//41, 39, 37, 35);
// 33, 31, 29, 27);
// 27, 29, 31, 33);

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

DeviceAddress temps[8];
char validadd[8];

void setup() {
  pinMode(RF_VCC, OUTPUT);
  pinMode(RF_GND, OUTPUT);
  pinMode(RF_DATA, OUTPUT);

  pinMode(LED, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  pinMode(53, OUTPUT);
  pinMode(51, OUTPUT);
  pinMode(49, OUTPUT);
  pinMode(47, OUTPUT);
  pinMode(45, OUTPUT);
  pinMode(43, OUTPUT);
  for (char i=0; i<8; i++)
  {
	pinMode(_bits[i], OUTPUT);
  }
  digitalWrite(53, LOW);
  digitalWrite(51, HIGH);
  digitalWrite(49, LOW);
  digitalWrite(45, LOW);

  // set up the LCD's number of columns and rows:
  //lcd.clear();
  // Print a message to the LCD.
  //lcd.display();
  //lcd.noCursor();
  //lcd.noBlink();
  Serial.begin(115200);
  Serial.println("Init");
  Serial.println("Testing RF sockets!");
  Serial.println("Dallas Temperature IC Control Library Demo");

  // Start up the library
  sensors.begin();
  // IC Default 9 bit. If you have troubles consider upping it 12.
  // Ups the delay giving the IC more time to process the temperature measurement

    // locate devices on the bus
  Serial.print("Locating devices...");
  Serial.print("Found ");
  char tempcount = sensors.getDeviceCount();
  Serial.print(tempcount, DEC);
  Serial.println(" devices.");
  for (char i=0; i<tempcount; i++)
  {
	//uint8_t *tmpadd;
	if (sensors.getAddress(temps[i], i))
	{
		validadd[i] = (char)TRUE;
		Serial.print("Sensor ");
		Serial.print(i+1, DEC);
		Serial.print(" address ");
		printAddress(temps[i]);
		//Serial.print(" contents ");
		Serial.println();
	}
	else
	{
		validadd[i] = (char)FALSE;
	}
  }
  
  // report parasite power requirements
  Serial.print("Parasite power is: ");
  if (sensors.isParasitePowerMode()) Serial.println("ON");
  else Serial.println("OFF");

  digitalWrite(RF_GND, LOW);

  lcd.begin(16, 2);

  initLCD();

  lcd.print(" Temp: -----");
  lcd.write(0xB2);
  lcd.print("C");

  lcd.createChar(1, emptyblk);
  lcd.createChar(2, solidblk);
  lcd.setCursor(14, 1);
  lcd.rightToLeft();
  for (char i=0; i<4; i++)
  {
	 lcd.write(2);
  }
  for (char i=0; i<4; i++)
  {
	 lcd.write(1);
  }
  lcd.leftToRight();
}

// function to print a device address
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    // zero pad the address if necessary
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX);
  }
}

void initLCD() {
	delay(40);
	sendChar(0x3C, 0);
	sendChar(0x0E, 0);
	sendChar(0x01, 0);
	delay(2);
	sendChar(0x06, 0);
}

void sendChar(uint8_t value, uint8_t mode)
{
	if (mode) {
		digitalWrite(_rs, HIGH);
	} else {
		digitalWrite(_rs, LOW);
	}
	digitalWrite(_enable, LOW);
	delayMicroseconds(1);
	for (char i=0; i<8; i++)
	{
	    digitalWrite(_bits[i], (value >> i) & 0x01);
	}
	digitalWrite(_enable, HIGH);
	delayMicroseconds(1);    // enable pulse must be >450ns
	digitalWrite(_enable, LOW);
	delayMicroseconds(100);   // commands need > 37us to settle
}

void ShortLong()
{
	digitalWrite(RF_DATA, HIGH);
	delayMicroseconds(190);
	digitalWrite(RF_DATA, LOW);
	delayMicroseconds(570);
}

void LongShort()
{
	digitalWrite(RF_DATA, HIGH);
	delayMicroseconds(570);
	digitalWrite(RF_DATA, LOW);
	delayMicroseconds(190);
}

void Send0()
{
	ShortLong();
	ShortLong();
}

void Send1()
{
	LongShort();
	LongShort();
}

void SendF()
{
	ShortLong();
	LongShort();
}

void DoSync()
{
	ShortLong();
	delayMicroseconds(770);
}

void sendID()
{
  SendF();
  SendF();
  SendF();
  Send0();
  Send0(); // remote ID
}

void chan1()
{
  SendF();
  SendF();
  SendF();
  Send0();
  Send1(); // chan 1
}

void chan2()
{
  SendF();
  SendF();
  SendF();
  Send1(); // chan 2
  Send0();
}

void chan3()
{
  SendF();
  SendF();
  Send1(); // chan 3
  Send0();
  Send0();
}

void chan4()
{
  SendF();
  Send1(); // chan 4
  SendF();
  Send0();
  Send0();
}

void chan5()
{
  Send1(); // chan 5
  SendF();
  SendF();
  Send0();
  Send0();
}

void SendOn()
{
  Send0();
  Send1(); // ON
}

void SendOff()
{
  Send1(); // OFF
  Send0();
}

unsigned long saveTime, timeTaken;

void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(1, 1);
  // print the number of seconds since reset:
  saveTime=millis();
  lcd.print(saveTime/1000);
  //Serial.println(saveTime/1000);
  //delay(500);
  //digitalWrite(LED, HIGH);
  //delay(500);
  //digitalWrite(LED, LOW);

  digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)

/*
  digitalWrite(LED2, LOW);
  digitalWrite(RF_DATA, HIGH);
  digitalWrite(RF_VCC, HIGH);
  delay(15);
  digitalWrite(RF_DATA, LOW);
  delay(15);
  for (char i=0; i<2; i++)
  {
	sendID();
	chan3();
	//chan4();
	SendOn();
	DoSync();
  }
  digitalWrite(RF_VCC, LOW);
  digitalWrite(LED2, HIGH);
*/
  digitalWrite(LED3, LOW);

  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
  Serial.print("Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("DONE");

  Serial.print("Temp 1 is: ");
  float temp1 = sensors.getTempC(temps[0]);
  Serial.println(temp1);
  lcd.setCursor(7, 0);
  lcd.print(temp1);
  
  Serial.print("Temp 2 is: ");
  Serial.println(sensors.getTempC(temps[1]));

  Serial.print("Temp 3 is: ");
  Serial.println(sensors.getTempC(temps[2]));
  digitalWrite(LED3, HIGH);
  
  timeTaken = millis()-saveTime;
  if (timeTaken < 1000)
  {
      delay(1000-timeTaken);               // wait for a second
  }
  saveTime = millis();
  digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW

  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(1, 1);
  // print the number of seconds since reset:
  lcd.print(millis()/1000);

  digitalWrite(RF_DATA, HIGH);
  digitalWrite(RF_VCC, HIGH);
  delay(15);
  digitalWrite(RF_DATA, LOW);
  delay(15);
  for (char i=0; i<2; i++)
  {
	sendID();
	chan3();
	//chan4();
	if (temp1 > 20.5) {
       digitalWrite(LED2, HIGH);
	   SendOff();
	} else if (temp1 < 20.2) {
       digitalWrite(LED2, LOW);
	   SendOn();
	}
	DoSync();
  }
  digitalWrite(RF_VCC, LOW);

  timeTaken = millis()-saveTime;
  if (timeTaken < 1000)
  {
      delay(1000-timeTaken);               // wait for a second
  }

}



